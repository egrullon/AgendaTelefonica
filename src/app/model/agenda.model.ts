import {TelefonoModel} from './telefono.model';

export class AgendaModel{

  constructor(public nombre: string, public fechaNacimiento: Date,  public edad: number, public telefonos: TelefonoModel[]) {
  }
}
