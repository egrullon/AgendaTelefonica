

export class TelefonoModel{

  constructor(public numero: string, public tipoTelefono: TipoTelefonoModel, public esPrincipal: boolean) {
  }
}

enum TipoTelefonoModel{
  Residencia = 'Residencia', Trabajo = 'Trabajo', Movil = 'Móvil', Otros = 'Otros'
}
