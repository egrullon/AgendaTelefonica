import { Component, OnInit } from '@angular/core';
import {AgendaModel} from '../model/agenda.model';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
  nombre: string;
  fechaNacimiento: Date;
  edad: number;
  agendas: AgendaModel[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onChangeAsignarEdad(event: any): void{
    this.fechaNacimiento = new Date(event.target.value);
    console.log(this.fechaNacimiento);
    // this.fechaNacimiento = new Date(this.fechaNacimiento);
    console.log('Ano ' + this.fechaNacimiento.getFullYear());
    this.edad = new Date().getFullYear() - this.fechaNacimiento.getFullYear();
  }

  onClickGuardarDatos(): void{
    console.log('guardando los datos');
    this.agendas.push({nombre: this.nombre, fechaNacimiento: this.fechaNacimiento, edad: this.edad, telefonos: []});
  }
}
